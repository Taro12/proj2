FROM node:16-alpine AS builder
WORKDIR /app
COPY ./frontend/package*.json ./
RUN npm install
COPY ./frontend/public/ ./public/
COPY ./frontend/src/ ./src/
RUN npm run build

FROM nginx:1-alpine
COPY --from=builder /app/build/ /usr/share/nginx/html
EXPOSE 80

# FROM node:16-alpine
# # RUN mkdir /app && chown node:node /app
# RUN ["npm", "install", "-g", "--unsafe-perm", "serve"]
# # USER node
# WORKDIR /app
# COPY --from=builder /app/build/ ./build
# # COPY --chown=node --from=builder /app/build/ ./build
# CMD ["serve", "-s", "build"]
